﻿using Foundation;
using MapKit;
using Nearby.Interfaces;
using Nearby.iOS.DependencyServices;
using Social;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(AppLauncher))]

namespace Nearby.iOS.DependencyServices
{
    public class AppLauncher : IAppLauncher
    {
        public bool OpenNativeMaps(double lat, double longitude, string place)
        {
            var coordinate = new CoreLocation.CLLocationCoordinate2D(lat, longitude);
            var address = new MKPlacemarkAddress();
            var mapPlcae = new MKPlacemark(coordinate, address);
            var mapItem = new MKMapItem(mapPlcae);

            mapItem.Name = place;

            mapItem.OpenInMaps();

            return true;
        }

        public bool SendTweet(string tweet)
        {
            bool success = false;

            //Check if user has twitter
            var slComposer = SLComposeViewController.FromService(SLServiceType.Twitter);
            if (slComposer == null)
            {
                new UIAlertView("Unavailable", "Twitter is not available, please sign in on your devices settings screen.", null, "OK").Show();
            }
            else
            {
                slComposer.SetInitialText(tweet);
                success = true;
                UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewControllerAsync(slComposer, true);
            }

            return success;
        }

        public bool OpenTwitterProfile(string username)
        {
            try
            {
                if (UIApplication.SharedApplication.OpenUrl(NSUrl.FromString($"twitter://user?screen_name={username}")))
                    return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Unable to launch url" + ex);
            }

            try
            {
                if (UIApplication.SharedApplication.OpenUrl(NSUrl.FromString($"tweetbot://{username}/timeline")))
                    return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Unable to launch url " + ex);
            }
            return false;
        }

        public bool OpenReviewsAppStore(string appURL)
        {
            try
            {
                if (UIApplication.SharedApplication.OpenUrl(NSUrl.FromString(appURL)))
                    return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Unable to launch review url" + ex);
            }

            return false;
        }

        public void OpenSettings()
        {
            UIApplication.SharedApplication.OpenUrl(new NSUrl(UIApplication.OpenSettingsUrlString));
        }
    }
}
