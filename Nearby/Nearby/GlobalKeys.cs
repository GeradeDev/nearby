﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nearby
{
    public class GlobalKeys
    {
        //Events API
        public const String EventfulAPIKey = "hTRdwhLvk8LgjnFL";
        public const String EventSearchCriteria = "music,comedy,family_fun_kids,movies_film,food,attractions,singles_social,performing_arts,sports,outdoors_recreation";

        //Open weather API
        public const String OpenWeatherAPIKey = "b806514997d45be8cf0f0000935b48e6";

        //Weather forecast API
        public const String DarkSkyKey = "d1b23eacfb99c26880a95fc3488ac11e";
        public const String DarkSkyExclusions = "currently,minutely,hourly,alerts,flags";

        //Store App IDs
        public const String AppStoreID = "1191495162";
        public const String PlayAppStoreID = "com.whatsapp";
    }
}
