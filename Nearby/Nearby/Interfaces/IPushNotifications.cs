﻿using System;
using System.Threading.Tasks;

namespace Nearby.Interfaces
{
    public interface IPushNotifications
    {
        void OpenSettings();
    }
}

