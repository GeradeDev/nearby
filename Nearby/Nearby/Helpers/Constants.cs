﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nearby.Helpers
{
    public static class Constants
    {
        // OAuth
        // For Google login, configure at https://console.developers.google.com/
        public static string ClientId = "384943135909-unhg0h7dfgpqtrh1boqfci3k4jv836vc.apps.googleusercontent.com";
        public static string ClientSecret = "BS0FcfGrXUvNSLpKiFtjfMYx";

        // These values do not need changing
        public static string Scope = "https://www.googleapis.com/auth/userinfo.email";
        public static string AuthorizeUrl = "https://accounts.google.com/o/oauth2/auth";
        public static string AccessTokenUrl = "https://accounts.google.com/o/oauth2/token";
        public static string UserInfoUrl = "https://www.googleapis.com/oauth2/v2/userinfo";


        // Set this property to the location the user will be redirected too after successfully authenticating
        public static string RedirectUrl = "http://www.warpdevelopment.co.za/";
    }


    public static class MessageKeys
    {
        public const string Message = "message";
        public const string Question = "question";
        public const string Choice = "choice";
    }
}
